#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Constants
# IDs
ID_PREFIX = 'M'
ID_MINIMAL_LENGTH = 4
ID_MAXIMAL_LENGTH = 4
ID_PATTERN = '^{:s}[0-9]{{{:d},}}$'.format(ID_PREFIX, ID_MINIMAL_LENGTH)
ID_TEMPORARY_PREFIX = 'T'

# Directories
DIRECTORIES = { # Relative paths on file system level (relative to the location of this Python file).
    'repo_dir'          : '../repository/',
    'hugo_dir'          : '../../content/model/',
    'hugo_asset_dir'    : '../../assets/media/model/', # Optimisable media (images, videos); path on file system level
    'hugo_static_dir'    : '../../static/media/model/', # Unoptimised files (GIFs, XMLs); path on file system level
}

# URLs
HUGO_ASSET_MEDIA_WEB_DIR = 'model/' # Base media directory for referencing optimised assets in Markdown and HTML code
HUGO_STATIC_MEDIA_WEB_DIR = 'media/model/' # Base media directory for referencing unoptimised static files in Markdown and HTML code
GITLAB_REPO_URL = 'https://gitlab.com/morpheus.lab/model-repo/' # Repo URL for referencing all original model files
#MORPHEUS_WEBSITE_BASE_URL = 'morpheus.gitlab.io'

# ID Reservations
JSON_FILENAME = 'id-reservations.json'

# File extensions
#REPO_ASSET_MEDIA_FILE_EXTENSIONS = ('jpg', 'jpeg', 'png', 'tiff', 'tif')
REPO_MEDIA_VIDEO_FILE_EXTENSIONS = ('avi', 'mp4', 'm4v', 'mov', 'webm', 'ogv') # File extensions that should be converted from GitLab video code to Hugo video code
REPO_STATIC_MEDIA_FILE_EXTENSIONS = ('gif', 'ode', 'tif', 'tiff', 'xml', 'zip') # File extensions of non-optimised files that are copied to Hugo's static folder
REPO_RESERVED_FILE_NAMES = ('index.md', 'model.md')

# Global variables
directories_abs = {} # Directories converted to absolute paths.
interactive_mode = True # User is prompted for input.

# Debugging
from IPython import embed

# Command line arguments
import argparse

# Logging
import logging

# Interactive mode
import prompter

# Files and Directories
import os, pathlib, shutil, urllib.parse

# Git
from pydriller import Repository
import datetime

# Parsing
import yaml
import re
import xml.etree.ElementTree as et
import json

# Model IDs
import sys
import hashlib

# Sorting
import pandas as pd
import numpy as np
from operator import itemgetter

# Statistics
import matplotlib.pyplot as plt
import matplotlib.ticker

def setup():
    """Performs some basic configurations for the program
    based on the command line arguments/flags passed by the user.
    """
    global interactive_mode

    # Pandas
    #pd.set_option('display.max_columns', None)
    #pd.set_option('display.max_rows', None)

    # Setup logging.
    logging_format = '[%(asctime)s] %(levelname)-8s %(name)20s.%(funcName)-26s %(message)s' # %(pathname)s:
    logging.basicConfig(format=logging_format) # Set logging_level=logging.WARNING for the root logger and all descendant loggers (such as from third party libraries)
    logger = logging.getLogger('Setup')
    logger.setLevel(logging.INFO) # Set logging level to INFO only for this logger

    # Read command line arguments
    # Parent parser with definitions of common arguments for all child parsers
    arg_parent_parser = argparse.ArgumentParser(add_help=False) # https://stackoverflow.com/questions/24666197/argparse-combining-parent-parser-subparsers-and-default-values

    # Adjust the degree of verbosity (log level)
    arg_group_verbosity = arg_parent_parser.add_mutually_exclusive_group()
    arg_group_verbosity.add_argument('-v', '--verbose', help='print debug messages', action='store_true')
    arg_group_verbosity.add_argument('-q', '--quiet', help='do not print anything unless an error occurs', action='store_true')
    # Path settings
    arg_parent_parser.add_argument('-r', '--repo-dir', metavar='<path>', default=DIRECTORIES["repo_dir"], help=f'set the directory of the models repository')

    # Child parser with subcommands that is actually invoked by the user
    arg_parser = argparse.ArgumentParser(description='Analyse the Morpheus model repository and generate Hugo markdown pages from it.')
    arg_subparsers = arg_parser.add_subparsers(title='subcommands', dest='subparser_name')
    
    # Subcommands
    # Build
    arg_parser_build = arg_subparsers.add_parser('build', aliases=['b'], parents=[arg_parent_parser], help='build Hugo markdown pages for the model repository section', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # These options define in advance what the programme should do in case of doubt. No user interaction necessary.
    arg_group_non_interactive = arg_parser_build.add_mutually_exclusive_group()
    arg_group_non_interactive.add_argument('-a', '--auto-assign', action='store_true', help='automatically assign valid model IDs where none has been defined yet')
    arg_group_non_interactive.add_argument('-c', '--validate', action='store_true', help='build the website only if all models are already assigned a valid ID, otherwise do not perform automatic ID corrections and halt')
    arg_group_non_interactive.add_argument('-t', '--temporary-ids', action='store_true', help='ignore missing or conflicting model IDs, use temporary IDs instead')
    
    # Model Statistics
    arg_parser_build.add_argument('-s', '--stats', action='store_true', help=f'Write model statistics to \'{DIRECTORIES["hugo_static_dir"]}stats/\'')
    
    # Reserve
    arg_parser_reserve = arg_subparsers.add_parser('reserve', aliases=['r'], parents=[arg_parent_parser], help='reserve model IDs', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    
    arg_parser_reserve_group = arg_parser_reserve.add_mutually_exclusive_group()
    arg_parser_reserve_group.add_argument('-n', metavar='n', type=int, nargs='?', default=1, const=1, help='number of IDs to be chosen automatically; only needed if IDs are not chosen manually')
    arg_parser_reserve_group.add_argument('-c', '--custom', metavar='IDs', nargs='+', help='choose ID(s) manually')
 
    arg_parser_reserve.add_argument('-p', '--print', action='store_true', help='just print available model IDs, don\'t actually reserve one')
    arg_parser_reserve.add_argument('-d', '--divisor', metavar='d', type=int, default=1, help='Select only \'round\' IDs, i.e. only those for which the numerical part \'a\' fulfils the condition \'a mod d == 0\'.')
    arg_parser_reserve.add_argument('-s', '--strategy', choices=['smallest', 'consecutive'], default='smallest', help='select whether the smallest possible IDs should be determined or the largest possible range of consecutive IDs; a possibly set \'divisor\' then only refers to the start ID')


    global args
    args = arg_parser.parse_args()

    global logging_level # Set different logging level seperately for each local handler to prevent logging spam from third party libraries (such as pydriller INFO log messages)
    if args.subparser_name:
        # Set logger verbosity settings (only when a subcommand was called)
        if args.quiet:
            logging_level = logging.ERROR
            logger.setLevel(logging_level)
        elif args.verbose:
            logging_level = logging.DEBUG
            logger.setLevel(logging_level)
            logger.info('Debug output turned on.')
        else:
            logging_level = logging.INFO
            logger.setLevel(logging_level)
        
        if args.subparser_name == 'build':
            # For build set/unset interactive mode
            if args.auto_assign or args.validate or args.temporary_ids:
                interactive_mode = False
            if args.temporary_ids:
                logger.info('Missing or conflicting model IDs will be replaced with random temporary IDs.')
    else:
        # In case the subcommand is missing, print help and exit
        arg_parser.print_help()
        sys.exit()

    # Set directory
    DIRECTORIES['repo_dir'] = args.repo_dir

    # Resolve absolute paths to the repo, Hugo and Hugo media directories.
    parser_dir = pathlib.Path(__file__).parent.resolve() # Also possible: os.path.dirname(os.path.realpath(__file__))
    for key, path in DIRECTORIES.items():
        absolute_path = pathlib.Path(parser_dir, path).resolve()
        directories_abs[key] = absolute_path

class MorpheusRepository:
    """Holds a Morpheus repository (initialised with the directory containing the repo)
    and provides methods for analysing it's structure.
    """

    def __init__(self, repo_dir, hugo_dir, hugo_asset_dir, hugo_static_dir):
        self.logger = logging.getLogger('MorpheusRepository')
        self.logger.setLevel(logging_level)

        self.repo_dir = repo_dir
        self.hugo_dir = hugo_dir
        self.hugo_asset_dir = hugo_asset_dir
        self.hugo_static_dir = hugo_static_dir

        self.models = [] # Metadata of all models
        self.category_info = [] # Non-model Markdown files
        self.reservations = [] # Metadata of reserved model IDs
        self.json_data = [] # JSON data of ID reservations that already exist

        # Analyse model repo
        self._analyse() # Collect metadata from repo structure and Markdown files
        self._read_reservations() # Load metadata of reserved model IDs

    
    def _analyse(self):
        """Goes through all subdirectories of the model repository and analyses the hierarchy.
        """
        # Find XML files.
        self.logger.debug('Searching for XML files...')
        repo_path = pathlib.Path(self.repo_dir)
        all_xml_files = list(repo_path.glob('**/*.xml'))

        # In case of more than one XML file in folder: Try to identify a main model by searching for special file names '*_main.xml', 'model.xml' or '_*.xml' (naming convention priority follows this order)
        main_xml_files = []
        for xml_file in all_xml_files:
            models_in_subfolder = list(xml_file.parent.glob('*.xml'))
            marked_models = [path for path in models_in_subfolder if path.name.lower().endswith('_main.xml')] or [path for path in models_in_subfolder if path.name.lower() == 'model.xml'] or [path for path in models_in_subfolder if path.name.startswith('_')] # Identify the main model (or a group with conflicting main models) with the highest priority naming scheme
            if not marked_models or xml_file in marked_models: # Either include all model files if there is no marked main model; or include model(s) of the highest prioriry naming scheme (MorpheusRepository.check_ids() will handle conflicts later if there is more than one model in either group)
                main_xml_files.append(xml_file)

        # Extract metadata for every main model (XML path, dates, model description, associated media, publication reference etc.)
        for xml_file in main_xml_files:
            self.logger.debug('Found model \'{}\'.'.format(xml_file.relative_to(self.repo_dir)))
            model_entry = {}

            # Get XML filename, full file path and relative path
            model_entry['xml_path_abs'] = xml_file
            model_entry['xml_path_rel'] = xml_file.relative_to(self.repo_dir) # Strip repo_dir

            # Extract the XML creation/publish date (author date) and the date of the last change (commit date) from the Git commit history.
            xml_path = model_entry['xml_path_rel'] # Use the main XML's Git commit history for the dates
            git_commits = list(Repository(path_to_repo=str(directories_abs['repo_dir']), filepath=xml_path).traverse_commits()) # Initialize model Git repo and get all commits for the XML file; continue listing the history of a file beyond renames as in 'git log --follow'
            if git_commits:
                model_entry['date'] = git_commits[0].author_date.isoformat() # Oldest author date as ISO 8601 formatted string (e. g. '2021-12-09T15:27:18+01:00'); https://stackoverflow.com/a/11857467/7192373
                model_entry['lastmod'] = git_commits[-1].committer_date.isoformat() # Newest commit date
                self.logger.debug(f'Extracted model\'s Git history')
            else:
                current_date = datetime.datetime.now(datetime.timezone.utc).replace(microsecond=0).isoformat()
                model_entry['date'] = current_date
                model_entry['lastmod'] = current_date
                self.logger.warning(f'\'{xml_file.relative_to(self.repo_dir)}\': Unable to extract (main) model dates from Git history because \'{xml_file.name}\' has not yet been committed in Git repo. Using current date \'{current_date}\' instead for now')

            # Check for additional Markdown files.
            md_files = sorted(xml_file.parent.glob('*.md'))
            if md_files:
                for md_file in md_files:
                    if md_file.name == 'index.md': # Model page.
                        self.logger.debug('Found Markdown file \'{}\' for model \'{}\'.'.format(pathlib.Path(md_file).name, xml_file.relative_to(self.repo_dir)))
                        model_entry['model_markdown_path_abs'] = md_file
                    elif md_file.name == 'model.md': # Optional content for 'Model' section on the model page.
                        self.logger.debug('Found Markdown file \'{}\' for model \'{}\'.'.format(pathlib.Path(md_file).name, xml_file.relative_to(self.repo_dir)))
                        model_entry['model_section_markdown_path_abs'] = md_file

            # Get all the media (images etc.)
            media_files = {attachment.name: attachment for attachment in xml_file.parent.glob('*') if attachment.name not in REPO_RESERVED_FILE_NAMES}
            model_entry['media'] = media_files
            
            # Extract further relevant metadata from index.md YAML frontmatter
            md_filepath = model_entry.get('model_markdown_path_abs')
            if md_filepath:
                with open(md_filepath, 'r') as f:
                    md_data = yaml.safe_load_all(stream=f)
                    yaml_optional_frontmatter = next(md_data)
                model_entry['id'] = yaml_optional_frontmatter.get('MorpheusModelID')
                model_entry['hidden'] = yaml_optional_frontmatter.get('hidden') # 'hidden' flag for yet unpublished models
                
                # Reference details
                publication = yaml_optional_frontmatter.get('publication')
                if publication:
                    # Check or complete some publication elements
                    publication.update(
                        {
                            'original_model'    : publication.get('original_model') if model_entry['xml_path_rel'].parent.parts[-3] != 'Contributed Examples' else None, # Must be neither True nor False in ‘Contributed Examples’, since it can be neither an original nor a reproduced model
                            'authors'           : publication.get('authors') or yaml_optional_frontmatter.get('authors'), # Author list of the YAML document can be overwritten with the explicit publication's ‘authors’ parameter, e.g. if a paper is cited in a reference section that has authors other than the Morpheus model (only permitted in the ‘Contributed Examples’ category)
                        }
                    )
                    publication_filtered = {key: value for key, value in publication.items() if not value in [None, '']} # Remove empty publication variables
                    if publication_filtered.keys() >= {'authors', 'title'}:
                        # An author and title is the minimum requirement for the ‘reference’ to appear
                        model_entry['publication'] = publication_filtered

                # Model statistics
                if 'stats' in args:
                    self.logger.debug('Collecting stats')
                    model_entry['category'] = list(xml_file.relative_to(self.repo_dir).parents)[-2] # Built-in Examples, Contributed Examples or Published Models   

            # Find all Markdown files that don't belong to a XML file (such as descriptions for categories like 'Built-in examples').
            # Main repo description.
            try:
                model_entry['markdown_repo'] = next(repo_path.glob('index.md')) # Find MD file for Models' main page.
            except StopIteration:
                pass

            # Main category description
            try:
                model_entry['category_markdown_path_abs'] = next(xml_file.parents[2].glob('index.md')) # Get the first and only possible occurrence of the main category MD file from the glob iterator.
                self.logger.debug('Found Markdown file for category \'{}\'.'.format(model_entry['category_markdown_path_abs'].relative_to(self.repo_dir)))
            except StopIteration:
                pass

            # Model group (such as 'Mouse', 'Zebrafish', 'CPM' etc.) description and metadata (especially 'weight's for sorting)
            try:
                model_entry['group_markdown_path_abs'] = next(xml_file.parents[1].glob('index.md'))
                self.logger.debug('Found Markdown file for group \'{}\'.'.format(model_entry['group_markdown_path_abs'].relative_to(self.repo_dir)))
            except StopIteration:
                pass

            self.models.append(model_entry)
    
        self.logger.info('Found {:d} models.'.format(len(self.models)))


    def _find_available_ids(self, n, strategy='smallest', divisor=1):
        """Determine available IDs

        Returns a list of ID strings formatted in the globally defined ID_PATTERN.
        If the search for the complete set of the requested ID range fails an incomplete list is returned.
        """

        available_ids = []

        if strategy == 'smallest':

            id_counter_numeric = divisor

            while len(available_ids) < n and id_counter_numeric < 10 ** ID_MAXIMAL_LENGTH:

                id_string = '{:s}{:0{:d}d}'.format(ID_PREFIX, id_counter_numeric, ID_MINIMAL_LENGTH) # Convert ID integer to a string according to the ID pattern.
                
                if not self._id_exists(id_string):
                    available_ids.append(id_string)
                id_counter_numeric += divisor

            """if len(available_ids) < n:
                # Empty list if only an incomplete ID set could be found
                available_ids = []"""

        else: # strategy == 'consecutive'

            id_counter_numeric = divisor # Start with 'divisor * 1'
            available_start_id = None
            range_length = 0
            max_range_length = 0 # Max achieved range length

            while range_length < n and id_counter_numeric < 10 ** ID_MAXIMAL_LENGTH:

                id_string = '{:s}{:0{:d}d}'.format(ID_PREFIX, id_counter_numeric, ID_MINIMAL_LENGTH)
                if not available_start_id:
                    # Save only the first ID (numeric part without prefix) of a range of IDs
                    available_start_id = id_counter_numeric

                if not self._id_exists(id_string):
                    range_length += 1
                    id_counter_numeric += 1
                else:
                    if range_length > max_range_length:
                        max_range_length = range_length
                        start_id_max_range_length = available_start_id # Save the start ID with the maximum available range length
                    available_start_id = None
                    range_length = 0

                    # Search for available range failed; calculate next start_id whilst taking into account divisor
                    id_counter_numeric += divisor - id_counter_numeric % divisor

            """if range_length == n:
                # Fill the list only if a complete ID set was found"""

            if range_length > max_range_length:
                # Check the lengths once again when the while loop breaks
                max_range_length = range_length
                start_id_max_range_length = available_start_id

            available_ids = ['{:s}{:0{:d}d}'.format(ID_PREFIX, id_counter_numeric, ID_MINIMAL_LENGTH) for id_counter_numeric in range(start_id_max_range_length, start_id_max_range_length + max_range_length)]

        return available_ids

    def _find_bad_ids(self):
        """Checks Markdown files and reservations file for bad formatted IDs

        Returns:
            - In case of bad ID(s) dict(s) with:
                - key: ID
                - value: dict with:
                    - key: 'markdown_invalid'
                    - value: filename
            - In case of correct ID(s) empty dict
        """

        pass

    def _find_duplicate_ids(self, location):
        """Checks Markdown files and reservations file for duplicate IDs

        Arguments:
            - 'location': where to check, i.e. 'markdown' or 'json'
 
        Returns:
            - In case of duplicate IDs:
                - Dict(s) with ID(s) as keys(s) containing
                    - list with:
                        - for 'location'=='markdown': name(s) of the file(s) in question
                        - for 'location'=='json': number of occurrences
            - In case of solely unique IDs:
                - Empty dict

        In Markdown files in particular, ID duplications can occur more,
        frequently as they are usually assigned manually by the user.

        Duplications are less likely to occur in reservations, as they
        are usually generated automatically and are easier to notice when
        editing manually in the central JSON file. In addition, IDs
        that are only *reserved* and not assigned to a model several times
        do not pose a problem for the model repository.
        """

        duplicate_ids_result = {}

        if location == 'markdown':
            # Check IDs in Markdown files

            # Convert model list into dataframe
            model_metadata = pd.DataFrame.from_dict(data=self.models).fillna(value=np.nan) # Convert Python's NoneType to NumPy NaN (otherwise, the DataFrame contains a mixture of None and NaN).

            # Find duplicate IDs
            models_with_ids = model_metadata[model_metadata['id'].notna()]
            markdown_duplicates_df = models_with_ids[models_with_ids.duplicated(subset=['id'], keep=False)].copy() # uniques = model_metadata[~duplicate]

            if not markdown_duplicates_df.empty:
                grouped_duplicates = markdown_duplicates_df[['id', 'xml_path_rel']].groupby(by='id') # Group duplicates by ID
                duplicate_ids_result = {id_element: sorted(group['xml_path_rel'].to_list(), key=lambda path: str(path).lower()) for id_element, group in grouped_duplicates} # Get list of file paths (sorted case-insensitively) per duplicate ID

        else:    
            # Check IDs in JSON reservation file

            id_reservations = pd.Series(self.reservations, dtype='string')
            json_duplicates_series = id_reservations[id_reservations.duplicated(keep=False)]

            if not json_duplicates_series.empty:
                json_duplicates_counts = json_duplicates_series.value_counts().to_dict() # Count the occurrences of IDs
                duplicate_ids_result = {id_element: count for id_element, count in json_duplicates_counts.items()}

        return duplicate_ids_result


    def _find_missing_ids(self):
        """Checks Markdown files for missing IDs

        Returns:
            - In case of missing ID(s) list of filename(s)
            - Otherwise empty dict
        """

        model_metadata = pd.DataFrame.from_dict(data=self.models).fillna(value=np.nan)
        filenames = model_metadata[model_metadata['id'].isna()]['xml_path_rel'].to_list()

        return filenames


    def _find_reserved_ids(self):
        """Checks Markdown files for reserved IDs

        Returns:
            - In case of reserved ID(s) dict(s) with:
                - key: ID
                - value: list with:
                    - filename(s)
            - Otherwise empty dict
        """
        
        reserved_ids_dict = {}
        for model in self.models:
            if 'id' in model and self._id_exists(model['id'], location='json'):
                if model['id'] in reserved_ids_dict:
                    reserved_ids_dict[model['id']].append(model['xml_path_rel'])
                else:
                    reserved_ids_dict[model['id']] = [model['xml_path_rel']]

        return reserved_ids_dict


    def _id_exists(self, id_string, location='any'):
        """Checks IDs against all assigned IDs in:
            - Markdown files and
            - reservations

        Arguments:
            - id_string(str): ID string including prefix and numeric part (e.g. 'M0123')
            - location: where to check, i.e. 'markdown', 'json', 'both' or 'any' (default: 'any')

        Returns:
            - In case of a unique ID: False
            - In case of a existing ID a location string (True):
                - 'markdown': ID was found in a markdown file that describes the model
                - 'reservation': ID was found in the JSON reservation file
                - 'both': ID was found in both places
        """

        if location == 'any':

            model_metadata = pd.DataFrame.from_dict(data=self.models).fillna(value=np.nan) # Convert model list into dataframe; replace Python's NoneType by NumPy NaN (otherwise, the DataFrame contains a mixture of None and NaN)

            if id_string in model_metadata['id'].values:
                return 'markdown'
            elif id_string in self.reservations:
                return 'reservation'
            else:
                return False

        elif location == 'markdown':

            model_metadata = pd.DataFrame.from_dict(data=self.models).fillna(value=np.nan)
            if id_string in model_metadata['id'].values:
                return True
            else:
                return False

        elif location == 'json':

            if id_string in self.reservations:
                return True
            else:
                return False
           

    def _read_reservations(self):
        """Loads reserved model IDs and corresponding metadata
        from a JSON reservations file

        Returns:
            - self.json_data: List of Dicts or, in case of an missing or empty JSON file, an empty list
        """

        # Load JSON file
        json_path = pathlib.Path(directories_abs['repo_dir'], JSON_FILENAME)
        try:
            with open(json_path, 'r') as json_file:
                self.json_data = json.load(json_file)
        except FileNotFoundError:
            self.logger.debug('File \'{:s}\' not found'.format(JSON_FILENAME))
            self.json_data = []
        except json.decoder.JSONDecodeError:
            self.logger.debug('Could not decode \'{:s}\''.format(JSON_FILENAME))
            self.json_data = []

        if self.json_data:
            # Get a list of reservations which should be considered in the ID search
            self.reservations = [list_item['id'] for list_item in self.json_data]


    def _write_reservations(self, new_ids):
        """Saves a list of model ID reservations
        to the JSON reservations file"""

        new_json_data = [{'id': id_element} for id_element in new_ids]
        self.json_data.extend(new_json_data)
        self.json_data.sort(key=itemgetter('id')) # Sort by ID

        # Save JSON
        json_path = pathlib.Path(directories_abs['repo_dir'], JSON_FILENAME)
        with open(json_path, 'w') as json_file:
            json.dump(self.json_data, json_file, indent=2)


    def check_ids(self):
        """Validates IDs in the given model repository, e. g.
            - identifying duplicate IDs in models' Markdown files
              as well as the reservation JSON file
            - finding models with missing IDs
            - automated assigning of IDs on request
        
        Returns:
            - dict: Dict of error messages with:
                - key: model ID
                - value: 
            - None: No errors
        """

        # Check Markdown IDs against reservations in JSON file
        reserved_ids = self._find_reserved_ids()
        taken_ids = {id_element: {'json_reserved': filenames} for id_element, filenames in reserved_ids.items()}

        # Check Markdown IDs for duplicates
        duplicate_markdown_ids = self._find_duplicate_ids(location='markdown')
        for id_element, filenames in duplicate_markdown_ids.items():
            if id_element in taken_ids:
                taken_ids[id_element]['markdown_duplicate'] = filenames
            else:
                 taken_ids[id_element] = {'markdown_duplicate': filenames}

        # Check reserved IDs for duplicates
        duplicate_json_ids = self._find_duplicate_ids(location='json')
        for id_element, count in duplicate_json_ids.items():
            if id_element in taken_ids:
                taken_ids[id_element]['json_duplicate'] = count
            else:
                 taken_ids[id_element] = {'json_duplicate': count}

        # Check for Markdown files without IDs
        missing_markdown_ids = self._find_missing_ids()
        if missing_markdown_ids:
            taken_ids[''] = missing_markdown_ids

        # Print validation result
        if taken_ids:
            if len(taken_ids) == 1:
                opening = f'There was a problem with 1 model ID:'
            else:
                opening = f'There were problems with {len(taken_ids):d} IDs:'

            id_blocks = []
            for id_element, issue_list in sorted(taken_ids.items()):

                output_issue_list = []

                if id_element == '':
                    id_element = 'No ID'
                    filenames = issue_list
                    filenames_string = ''.join(f'\n      - \'{filename}\'' for filename in filenames)
                    output_issue_list.append(f'ID is not assigned to {len(filenames):d} model(s): {filenames_string}')

                if 'markdown_duplicate' in issue_list:

                    filenames = issue_list['markdown_duplicate']
                    filenames_string = ''.join(f'\n      - \'{filename}\'' for filename in filenames)
                    output_issue_list.append(f'ID is assigned to {len(filenames):d} models: {filenames_string}')
                
                if 'json_reserved' in issue_list:

                    filenames = issue_list['json_reserved']
                    filenames_string = ''.join(f'\n      - \'{filename}\'' for filename in filenames)
                    output_issue_list.append(f'ID is already reserved. Model(s) concerned: {filenames_string}')
                
                if 'json_duplicate' in issue_list:

                    count = issue_list['json_duplicate']
                    output_issue_list.append(f'ID is reserved {count:d} times')

                issue_block = '\n    '.join(f'- {list_element:s}' for list_element in output_issue_list)
                id_block = f'  {id_element:s}:\n    {issue_block:s}'

                id_blocks.append(id_block)
            
            combined_id_blocks = '\n\n'.join(f'{id_block:s}' for id_block in id_blocks)
            output = f'Validation failed\n\n{opening}\n\n{combined_id_blocks:s}\n'

            if args.validate:
                return output
            else:
                self.logger.warning(output)

        # Auto-assignment of missing IDs
        if missing_markdown_ids and (args.auto_assign or interactive_mode):
            self.logger.debug('Searching for available IDs...')
            smallest_free_ids = self._find_available_ids(n=len(missing_markdown_ids)) # determine a list containing available IDs which are as small as possible
            self.logger.debug('Determined the {:d} smallest possible available IDs.'.format(len(smallest_free_ids)))

            if interactive_mode:
                args.auto_assign = prompter.yesno('\nShould valid IDs be assigned automatically?')
                print()

            if args.auto_assign:
                # Assign the found IDs to the models
                # Adjust model metadata for the Hugo markdown generation.
                smallest_free_ids = pd.Series(smallest_free_ids)
                model_metadata = pd.DataFrame.from_dict(data=self.models).fillna(value=np.nan)
                model_metadata.loc[model_metadata['id'].isna(), 'id'] = smallest_free_ids.values # Overwrite all NaNs in ID column with IDs.
                model_metadata = model_metadata.where(cond=pd.notna(model_metadata), other=None) # Use NoneType instead of np.nan in the dict again.
                self.models = model_metadata.to_dict('records')
                self.logger.debug('Assigned {:d} IDs.'.format(len(smallest_free_ids)))
                
                # Insert model IDs also into user's repo index.md files.
                # ...

            elif interactive_mode: # (and not args.auto_assign)
                 print('The model pages will be generated with temporary IDs.\n')


    def create_hugo_documentation(self):
        """Initialises models' content and media folders for the Hugo website
        """

        # Delete existing Hugo content and media folders.
        for path in [self.hugo_dir, self.hugo_asset_dir, self.hugo_static_dir]:
            if os.path.exists(path):
                self.logger.debug('Deleting existing Hugo folder \'{}\'...'.format(path))
                shutil.rmtree(path)

        # Create new Hugo 'models' directory.
        self.logger.debug('Creating Hugo \'Models\' section...')
        os.makedirs(self.hugo_dir)

        # Create YAML file.
        # YAML standard frontmatter
        yaml_file = {}
        yaml_file['frontmatter'] = {
            'layout'    : 'docs',
            'title'     : 'Models',
        }

        # Look for an optional YAML document
        md_files = {path.name: path for path in self.repo_dir.glob('*.md')} # create dict with file name as key and full path as value
        if 'index.md' in md_files:
            with open(md_files['index.md'], 'r') as f:
                md_data = yaml.safe_load_all(stream=f)
                yaml_data = next(md_data) # first part: YAML frontmatter
                if isinstance(yaml_data, dict):
                    yaml_file['frontmatter'].update(yaml_data) # update standard frontmatter with custom one from Markdown file
                
                # get content without frontmatter
                f.seek(0)
                file_text = f.read()
                yaml_file['content'] = re.sub(r'\s*---.*---\s*', '', file_text, flags=re.MULTILINE|re.DOTALL) # https://stackoverflow.com/a/9756545/7192373

        # Save YAML file as '_index.md'
        page_path = os.path.join(self.hugo_dir, '_index.md')
        with open(page_path, 'w') as f:
            yaml.dump_all([yaml_file['frontmatter']], stream=f, explicit_start=True)
            f.write('---\n')
            f.write(yaml_file.get('content', '')) # write empty string in case there is no additional index.md content

    def create_stats(self):
        pd.set_option('display.max_rows', None)
        df = pd.DataFrame(self.models)
        df = df.loc[:, ['date', 'id', 'category']] # 'lastmod', 'publish_date'
        
        # dtype conversions
        df[['date']] = df[['date']].apply(pd.to_datetime, utc=True) # 'lastmod', 'publish_date'
        df['category'] = df['category'].astype('string')

        # Group and count by category
        df.sort_values(by='date', inplace=True) # Chronological order is important for the cumulative sums
        df['ones'] = 1 # Add column with '1's to enable cumsum calculation
        categories = df.groupby('category')
        category_totals = []
        for name, group in categories:
            df[name] = group['ones'].cumsum() # Alternative short name version: short_name = name.split(' ')[0].lower(); column_label = f'total_{short_name}'
            category_totals.append(name) # Save column labels for later

        # Choose 'date' column as index
        df.set_index('date', inplace=True)

        # Add data row with current date in order to plot up to the present
        now = datetime.datetime.now(datetime.timezone.utc).replace(microsecond=0) # Current date with microseconds dropped
        df_now = pd.DataFrame(index=[now])
        df = pd.concat([df, df_now]) # Append a datarow with the current UTC date so that the x-axis always ends today
 
        # Drop duplicate dates
        df.fillna(method='ffill', inplace=True) # Forward fill the NaNs, otherwise values could be lost when removing duplicate dates
        df_unique = df.loc[~df.index.duplicated(keep='last')] # Drop duplicate dates while keeping the last one (with the highest cumsum), i.e. keep all rows that duplicated() evaluates to False; an unique index is necessary for Resampler.pad (to forward fill values upon date resampling)

        # Carry out resampling and stepwise imputation over whole timeline including all categories
        df_unique = df_unique.resample('M').ffill() # Convert frequency of time series ('M' = month end, 'MS' = month start) and perform imputation with forward-fill/pad
        df_unique[category_totals] = df_unique[category_totals].fillna(value=0).astype(int) # Replace leading NaNs with zeros (for plotting) that were not forward filled with pad(); in the case of 'MS' resampling, NaNs are also added in the newly generated first data row

        # Plot
        ax = df_unique[category_totals].plot.area(stacked=True)
        plt.title('Morpheus Model Repository')
        plt.xlabel('Date')
        plt.ylabel('Models by Category')
        plt.legend(loc='upper left')
        ax.yaxis.set_minor_locator(matplotlib.ticker.AutoMinorLocator())
        plt.grid(visible=True, which='major', axis='y')
        ax.set_aspect(0.25)

        # Save plot
        stats_folder = pathlib.Path(self.hugo_static_dir, 'stats')
        matpotlib_file_path = pathlib.Path(stats_folder, 'models-by-category') # URL: /media/model/stats/models-by-category.*

        self.logger.debug(f'Writing plot \'{matpotlib_file_path}\'...')
        os.makedirs(stats_folder, exist_ok=True)
        plt.savefig(fname=f'{matpotlib_file_path}.svg', bbox_inches='tight')
        plt.savefig(fname=f'{matpotlib_file_path}.png', bbox_inches='tight', dpi=150)
        plt.savefig(fname=f'{matpotlib_file_path}.pdf', bbox_inches='tight')


    def reserve_ids(self):
        """Handles ID reservations invoked by the 'reserve' subcommand
        """

        if args.print:

            # Just print the IDs, don't write the reservation(s) to the JSON file

            available_ids = self._find_available_ids(args.n, args.strategy, args.divisor)
            print(f'\nAvailable IDs: {available_ids}')

        else:
            # No 'print' argument given; write reservation(s)

            if args.custom:
                # Custom IDs passed by the user

                args.custom.sort()

                available_ids = [id_element for id_element in args.custom if not self._id_exists(id_element)]
                unavailable_ids = [{'id': id_element, 'location': self._id_exists(id_element)} for id_element in args.custom if self._id_exists(id_element)] # filter(self._id_exists, args.custom)

                """unavailable_ids = []
                for id_element in args.custom:
                    result = self._id_exists(id_element)
                    if result:
                        unavailable_ids.append((id_element, result))"""

                if len(available_ids) < len(args.custom):
                    # There were unavailabe IDs

                    # Separation of unavailabe IDs by location for output
                    ids_markdown = ', '.join([id_element['id'] for id_element in unavailable_ids if id_element['location'] == 'markdown'])
                    ids_reserved = ', '.join([id_element['id'] for id_element in unavailable_ids if id_element['location'] == 'reservation'])

                    # Addapt grammar of output to number of available/unavailable IDs
                    if len(available_ids) == 0:

                        if ids_markdown and ids_reserved:
                            self.logger.error(f'None of {len(args.custom)} requested IDs is still availabe. IDs taken: {ids_markdown} (assigned to a model); {ids_reserved} (already reserved). Please try again')
                        elif ids_markdown and not ids_reserved:
                            self.logger.error(f'None of {len(args.custom)} requested IDs is still availabe. IDs taken: {ids_markdown} (assigned to a model). Please try again')
                        else:
                            self.logger.error(f'None of {len(args.custom)} requested IDs is still availabe. IDs taken: {ids_reserved} (already reserved). Please try again')

                    else:

                        if len(available_ids) == 1:
                            output_string_be = 'is'
                        else:
                            output_string_be = 'are'

                        if ids_markdown and ids_reserved:
                            self.logger.error(f'Only {len(available_ids)} out of {len(args.custom)} requested IDs {output_string_be} still availabe: {available_ids}. IDs taken: {ids_markdown} (assigned to a model); {ids_reserved} (already reserved). Please try again')
                        elif ids_markdown and not ids_reserved:
                            self.logger.error(f'Only {len(available_ids)} out of {len(args.custom)} requested IDs {output_string_be} still availabe: {available_ids}. IDs taken: {ids_markdown} (assigned to a model). Please try again')
                        else:
                            self.logger.error(f'Only {len(available_ids)} out of {len(args.custom)} requested IDs {output_string_be} still availabe: {available_ids}. IDs taken: {ids_reserved} (already reserved). Please try again')
                else:

                    self._write_reservations(available_ids)

                    if len(available_ids) == 1:
                        self.logger.info(f'ID available and succesfully reserved: {", ".join(available_ids)}')
                    else:
                        self.logger.info(f'All {len(available_ids):d} IDs available and succesfully reserved: {", ".join(available_ids)}')

            else:
                # User asked to automatically find availabe IDs

                available_ids = self._find_available_ids(args.n, args.strategy, args.divisor)

                if len(available_ids) == args.n:

                    self._write_reservations(available_ids)

                    if args.n == 1:
                        self.logger.info('1 available ID found and succesfully reserved: {}'.format(', '.join(available_ids)))
                    else:
                        self.logger.info('{:d} available IDs found and succesfully reserved: {}'.format(args.n, ', '.join(available_ids)))

                else:

                    self.logger.error('Only {:d} out of {:d} requested available IDs found. Please try again.'.format(len(available_ids), args.n))


class MorpheusModel:
    """Holds a Morpheus model including the XML file, metadata and corresponding generated Hugo web page.
    """
    def __init__(self, model_data, hugo_dir, hugo_asset_dir, hugo_static_dir):
        self.logger = logging.getLogger('MorpheusModel')
        self.logger.setLevel(logging_level)

        self.model_data = model_data
        self.hugo_dir = hugo_dir
        self.hugo_asset_dir = hugo_asset_dir
        self.hugo_static_dir = hugo_static_dir

    def markdownify(self):
        """Generates Hugo Markdown file from a given Morpheus model dict.
        """
        self._create_subsection()
        self._create_model_page()

    def _create_subsection(self):
        """Creates subsections that are displayed on the models landing page
        according to the directory levels of the repository.
        """
        # Create subsection folders and corresponding YAML files if neccessary
        for pos, subfolder in enumerate(self.model_data['xml_path_rel'].parent.parts):
            
            hugo_subpath_rel = pathlib.Path(*self.model_data['xml_path_rel'].parent.parts[:pos+1]) # Consecutively combine subsections to one path when diving deeper into the directory tree
            hugo_subpath_abs = pathlib.Path(self.hugo_dir, hugo_subpath_rel)
            
            if not os.path.exists(hugo_subpath_abs):
                self.logger.debug('Creating Hugo subfolder \'{}\'...'.format(hugo_subpath_rel))
                os.makedirs(hugo_subpath_abs)

            hugo_page_path_abs = pathlib.Path(hugo_subpath_abs, '_index.md')

            if (pos == 0 or pos == 1 and not self.model_data.get('hidden')) and not os.path.isfile(hugo_page_path_abs): # Two highest subsections ('Built-in Examples', 'Contributed Examples' etc. and 'ODE', 'PDE' etc.); don't save '_index.md' file if the model behind its folder is flagged as 'hidden' or an '_index.md' already exists in that folder
                # Create special non-model '_index.md' YAML file for a category, including a 'menu' entry
                yaml_document = {
                    'frontmatter': {
                        'type'    : 'book',
                        'title'     : subfolder,
                        'summary'   : ' ', # There has to be a summary in order to display the main 'Models' page correctly
                    }
                }

                # Retrieve additional YAML metadata/content for highest category ('Built-in Examples', 'Contributed Examples' etc.) as well as for model groups ('Mouse', 'Zebrafish', 'CPM' etc.) from Markdown file if given
                category_markdown_path_abs = self.model_data.get('category_markdown_path_abs') # Path to MD file containing content for the 'Overview' page of categories such as "Built-in Examples"
                group_markdown_path_abs = self.model_data.get('group_markdown_path_abs')
                markdown_path_abs = None
                if (pos == 0 and category_markdown_path_abs):
                    markdown_path_abs = category_markdown_path_abs
                elif (pos == 1 and group_markdown_path_abs):
                    markdown_path_abs = group_markdown_path_abs

                if markdown_path_abs:
                    with open(markdown_path_abs, 'r') as f:
                        # Parse frontmatter.
                        md_data = yaml.safe_load_all(stream=f)
                        yaml_optional_frontmatter = next(md_data) # First part is the custom YAML frontmatter.

                        # Combine generated and user-defined frontmatter
                        yaml_document['frontmatter'].update(yaml_optional_frontmatter) # Add YAML dict containing additional frontmatter. Values of keys that both exist in the generated and optional frontmatter will be overwritten by the values of the optional (custom) frontmatter.
                        
                        # Remove frontmatter, leave only content.
                        f.seek(0) # Jump to beginning of file.
                        file_text = f.read()
                        content = re.sub(r'^\s*---.*---\s*$', '', file_text, flags=re.MULTILINE|re.DOTALL) # https://stackoverflow.com/a/9756545/7192373
                        yaml_document['content'] = ('{:s}').format(content)

                    # Get all the media (featured images etc.)
                    repo_category_path = pathlib.Path(directories_abs['repo_dir'], hugo_subpath_rel)
                    media_files = {attachment.name: attachment for attachment in repo_category_path.glob('*.*') if attachment.name not in REPO_RESERVED_FILE_NAMES} # glob for '*.*', i.e. only files, not subfolders
                    hugo_category_path = pathlib.Path(self.hugo_dir, hugo_subpath_rel)
                    for media_file_name, media_file_path in media_files.items():
                        self.logger.debug('Writing media file \'{}\'...'.format(pathlib.Path(hugo_subpath_rel, media_file_name)))
                        shutil.copy(media_file_path, hugo_category_path)

                # Save YAML file as '_index.md'
                self.logger.debug('Writing YAML file \'{}\'...'.format(pathlib.Path(hugo_subpath_rel, '_index.md')))
                with open(hugo_page_path_abs, 'w') as f: # https://pyyaml.org/wiki/PyYAMLDocumentation
                    yaml.dump_all([yaml_document['frontmatter']], stream=f, explicit_start=True)
                    f.write('---') # Hugo Academic expects '---' at the end of the YAML front matter
                    f.write(yaml_document.get('content', '\n{{< list_children >}}')) # Write optional subcategory/overview content if present, otherwise just list the models and their summaries

    def _create_model_page(self):
        """Generates a Hugo Markdown file including:
        - the YAML front matter for hugo Academic,
        - information from the model XML file, e.g. Description/Title, Description/Details
        - the highlighted XML code.
        """
        # Parse XML
        try:
            tree = et.parse(self.model_data['xml_path_abs'])
        except et.ParseError as e:
            # In case of an invalid XML file, exit the script
            self.logger.error(f"{self.model_data['id']}: Invalid XML file \'{self.model_data['xml_path_rel']}\': \'{str(e)}\'")
            sys.exit(1)

        root = tree.getroot()
        try:
            model_title = root.find('Description').find('Title').text
        except AttributeError: # If the XML tag is missing, this error is raised: "AttributeError: 'NoneType' object has no attribute 'text'"
            model_title = ''
        try:
            model_details = root.find('Description').find('Details').text
        except AttributeError:
            model_details = ''

        # Create the model's YAML documents
        yaml_frontmatter_document = {
            'aliases'           : [], # Automatically generated alternative spellings such as the plural form ('model'/'models') in the URL will be added later
            'date'              : self.model_data['date'], # Date of first Git commit
            'lastmod'           : self.model_data['lastmod'], # Date of latest Git commit
            'linktitle'         : self.model_data['xml_path_rel'].parent.parts[-1],
            'menu'              : {
                self.model_data['xml_path_rel'].parent.parts[-3]: {
                    'parent'    : self.model_data['xml_path_rel'].parent.parts[-2],
                },
            },
            'tags'              : [], # Automatically generated tags will be added later
            'title'             : model_title,
            'toc'               : 'true',
            'type'              : 'book',
        }

        # Persistent MorpheusModelID; If none given, generate a temporary ID using a SHA-1 hash of the XML file content.
        model_id = self.model_data.get('id')
        if not model_id:
            # Read XML file in 64 kB chunks; https://stackoverflow.com/a/22058673/7192373
            buf_size = 65536
            sha1 = hashlib.sha1()
            with open(self.model_data['xml_path_abs'], 'rb') as f:
                while True:
                    data = f.read(buf_size)
                    sha1.update(data)
                    if not data:
                        break

            sha1_decimal = int(sha1.hexdigest(), 16) % (10**4) # Use last 4 digits of the SHA1 decimal value
            model_id = '{:s}{:04d}'.format(ID_TEMPORARY_PREFIX, sha1_decimal)
            yaml_frontmatter_document.update({'morpheusmodelid': model_id}) # Add generated model ID to YAML frontmatter

        # Add model ID as slug, regardless if it's a persistent or temporary ID
        yaml_frontmatter_document.update({'slug': model_id})

        # Retrieve additional YAML frontmatter and page content from Markdown file if given.
        content = {}

        # Read main page content
        model_markdown_path_abs = self.model_data.get('model_markdown_path_abs') # Path to MD file
        if model_markdown_path_abs: # Open optional Markdown file
            with open(model_markdown_path_abs, 'r') as f:
                # Parse frontmatter
                md_data = yaml.safe_load_all(stream=f)
                yaml_optional_frontmatter = next(md_data) # First part is the custom YAML frontmatter

                # Combine generated and user-defined frontmatter
                forbidden_user_keys = ['date', 'lastmod'] # YAML variables that the user is not allowed to set manually, but only this script
                yaml_optional_frontmatter = {key.lower(): value for key, value in yaml_optional_frontmatter.items()} # Ensure that all user YAML variables are lower case so that duplicates do not occur in Dict item substitutions nor item deletions fail
                for key in forbidden_user_keys:
                    if key in yaml_optional_frontmatter: # Delete all forbidden user-defined Dict items
                        del yaml_optional_frontmatter[key]
                yaml_frontmatter_document.update(yaml_optional_frontmatter) # Add YAML dict containing additional frontmatter. Values of keys that both exist in the generated and optional frontmatter will be overwritten by the values of the optional (custom) frontmatter

                # Remove frontmatter, leave only content
                f.seek(0) # Jump to beginning of file
                file_text = f.read()
                content['page'] = re.sub(r'^\s*---.*---\s*$', '', file_text, flags=re.MULTILINE|re.DOTALL) # https://stackoverflow.com/a/9756545/7192373
        
        # Read contents of 'model.md' for 'Model' subheading
        content['model'] = ''
        model_section_markdown_path_abs = self.model_data.get('model_section_markdown_path_abs') # Path to MD file containing content for the 'Model' subheading.
        if model_section_markdown_path_abs:
            with open(model_section_markdown_path_abs, 'r') as f:
                content['model'] = f.read()

        # Extract or manipulate some page contents
        # Try to extract a summary text
        # For displaying a summary text on the model page, Markdown blockquote syntax is used; Wowchemy: https://wowchemy.com/docs/content/writing-markdown-latex/#blockquote
            # Example: '> Summary text...'
        #pattern = (
        #    r'^\s*> *'  # At start of string ('^'), match any leading whitespace ('\s*') (because page content starts with '\n'), followed by character '>', followed by any space character(s) (' *')
        #    r'(.*)'     # Capture any following characters ('.*')
        #    r'\s*'      # Match trailing white space including new lines ('\s*')
        #)
        #match = re.search(pattern, content['page']) # Match first occurence
        #if match:
        #    summary_text = match.group(1)
        #content['page'] = re.sub(pattern, '', content['page'], 1) # Remove summary text

        # Manipulate page contents in both 'page' and 'model' parts
        asset_media_web_dir = pathlib.Path(HUGO_ASSET_MEDIA_WEB_DIR, model_id.lower())
        static_media_web_dir = pathlib.Path(HUGO_STATIC_MEDIA_WEB_DIR, model_id.lower())
        for part in content:
            # Videos (GitLab: https://docs.gitlab.com/ee/user/markdown.html#videos; Academic/Wowchemy: https://wowchemy.com/docs/writing-markdown-latex/#videos)
            # Example: '![](SisterReunionMovie.mp4)'
            pattern = ( 
                r'!\[(?P<alt>.*)\]'                                                 # The string starts with '![]' containig a subpattern with any characters except \n for the alt text inside the brackets. Also enclose the whole pattern with parentheses to have it returned in the re.findall() tuple.
                fr'\((?P<src>.+\.({"|".join(REPO_MEDIA_VIDEO_FILE_EXTENSIONS)}))'   # After the left parenthesis extract the subpattern, which is the file name consisting of at least one character a literal dot and a video file extension.
                r'\s*\)'                                                            # The video code ends with a closing parenthesis and perhaps some spaces before that.
            )
            content[part] = re.sub(
                pattern,
                r'{{{{< video src="{}/\g<src>" controls="yes" >}}}}'.format(asset_media_web_dir), # Use named backreferences to groups; 'alt="\g<alt>"' is not supported for videos by Hugo/Academic
                content[part]
            )

            # Figures: Convert general markdown code (parsed by GitLab) to Hugo Academic's notation (https://sourcethemes.com/academic/docs/writing-markdown-latex/#images); Search for markdown image code; https://developers.google.com/edu/python/regular-expressions.
            # Example: '![](cellcycle_singlecell.png "Time plots of ODE model of *Xenopus* embryonic cell cycle.")'
            
            pattern = ( 
                r'!\[(?P<alt>.*?)\]'    # The string starts with '![]' containig a subpattern with any characters except \n for the alt text inside the brackets; also enclose the whole pattern with parentheses to have it returned in the re.findall() tuple; '.*?' means: don't be greedy and stop as early as possible before a closing bracket (otherwise nested markdown occurring later in the link title would be matched by the regex)
                r'\((?P<src>.+?)'       # After the left parenthesis extract the subpattern, which is the file name consisting of any character (except for line terminators) between one and unlimited times, as few times as possible
                r'( +'                  # If a caption is given, the subpattern maybe ends with space(s).
                r'["\']'                # An optional caption starts with a quotation mark (single or double quotes).
                r'(?P<caption>.*?)'     # The second subpattern contains any image caption. '.*?' means: don't be greedy and stop before the quotation mark (which is a perl compatible regular expression). Non-PCRE Alternative: '[^"\']*'.
                r'["\'])?'              # The image caption ends with a quotation mark.
                r' *\)'                 # The image code ends with a closing parenthesis and perhaps some spaces before that.
            )

            if part == 'page':
                # Identify featured image which is displayed by Hugo/Wowchemy as a thumbnail in compact list views
                page_folder = pathlib.Path(self.hugo_dir, self.model_data['xml_path_rel'].parent)
                media = self.model_data.get('media')
                hugo_not_an_image = ['svg']
                media_file_stems = {media_file_path.stem: media_file_path for media_file_path in media.values()}
                if 'featured' in media_file_stems.keys() and media_file_stems['featured'].suffix.lstrip('.') not in hugo_not_an_image:
                    # If featured.* image file exists, copy file to destination Markdown folder
                    self.logger.debug('Writing user-defined featured image \'{}\'...'.format(pathlib.Path(page_folder, media_file_stems['featured'].name)))
                    shutil.copy(media_file_stems['featured'], page_folder)
                else:
                    # Iterate through referenced figures as long as no valid image is found and referenced figures (matches) are left
                    match = re.search(pattern, content[part])
                    featured_image_found = False
                    while match and not featured_image_found:
                        # If 'featured.*' image file is not present, use the first embedded image (that has an allowed suffix) as featured image
                        featured_image_name = match.group('src')
                        featured_image_path = self.model_data['media'][featured_image_name]
                        if featured_image_path.suffix.lstrip('.') not in hugo_not_an_image:
                            self.logger.debug('Writing automatically selected featured image \'{}\'...'.format(pathlib.Path(page_folder, featured_image_name)))
                            shutil.copy(featured_image_path, pathlib.Path(page_folder, f'featured{featured_image_path.suffix}')) # Copy and rename to 'featured.<suffix>'
                            featured_image_found = True
                        else:
                            match = re.search(pattern, content[part])

            # Replace with Hugo figure shortcodes using file paths depending on whether the image file is a Hugo-optimised 'asset' file or non-optimised 'static' file
            match = re.search(pattern, content[part])
            while match:
                file_extension = pathlib.Path(match.group('src')).suffix.lstrip('.')
                if file_extension not in REPO_STATIC_MEDIA_FILE_EXTENSIONS:
                    replacement = rf'{{{{< figure src="{asset_media_web_dir}/\g<src>" caption="\g<caption>" alt="\g<alt>" >}}}}' # Use named backreferences to groups.
                else:
                    replacement = rf'{{{{< figure src="../../{static_media_web_dir}/\g<src>" caption="\g<caption>" alt="\g<alt>" >}}}}'
                content[part] = re.sub(pattern, replacement, content[part], count=1)
                match = re.search(pattern, content[part])

            # Attachments: Convert links in the model description pointing to files in the GitLab model folder to links pointing to Hugo's static files directory
            # Example: '[domain]: domain.tiff'
            pattern = (
                r'^\[(?P<link_name>.*)\]:\s*'   # '^': whole pattern has to be at the start of a line; any single character except '\n' in brackets, followed by a colon and one or more whitespace characters ('[ \n\r\t\f]')
                r'(?!https?://)'                # exclude 'http(s)' by using a negative lookahead; https://stackoverflow.com/a/1240365/7192373
                r'(?P<file_name>\S*\.\S*)'      # matches file with any non-whitespace characters separated by a string
            )
            model_static_dir = urllib.parse.urljoin(HUGO_STATIC_MEDIA_WEB_DIR, ''.join((model_id.lower(), '/'))) # Construct URL to model's static media directory with trailing '/'
            content[part] = re.sub(
                pattern,
                lambda match: f'[{match.group("link_name")}]: ../../{urllib.parse.quote(urllib.parse.urljoin(model_static_dir, match.group("file_name")))}', # https://docs.python.org/3/library/re.html#re.sub; # f'[\g<linkname>]: {file_src_dir}/\g<filename>'; Alternative with shortcode instead of relativ path: lambda match: f'[{match.group(1)}]: {{{{< model_download "{urllib.parse.quote(urllib.parse.urljoin(model_static_dir, match.group(2)))}" "url_only" />}}}}'
                content[part]
            )

            # LaTeX: Convert GitLab-flavored Markdown (https://docs.gitlab.com/ee/user/markdown.html#math) to Hugo/Wowchemy Markdown
            # Example: '$`a^2+b^2=c^2`$ -> $a^2+b^2=c^2$' (inline)
            pattern = r'\$`(?P<latex>.*?)`\$' # Named capture group 'latex' (.*?) matches any character ('.') (except for line terminators) between zero and unlimited times (*), as few times as possible (?)
            replacement = r'$\g<latex>$'
            content[part] = re.sub(pattern, replacement, content[part])

        if not content.get('page'): # No Markdown file given or Markdown file doesn't contain page contents; use description from XML file
            if model_details: # Use XML description
                content['page'] = model_details
            else:
                content['page'] = ''

        # Write all media files
        asset_media_folder = pathlib.Path(self.hugo_asset_dir, model_id.lower())
        static_media_folder = pathlib.Path(self.hugo_static_dir, model_id.lower())
        media = self.model_data.get('media') # Dict with names and paths of all media files
        if media:
            for media_file_name, media_file_path in media.items():
                file_extension = media_file_path.suffix.lstrip('.')
                if file_extension not in REPO_STATIC_MEDIA_FILE_EXTENSIONS:
                    self.logger.debug('Writing \'asset\' media file \'{}\'...'.format(pathlib.Path(asset_media_folder, media_file_name)))
                    os.makedirs(asset_media_folder, exist_ok=True)
                    shutil.copy(media_file_path, asset_media_folder)
                else:
                    self.logger.debug('Writing \'static\' media file \'{}\'...'.format(pathlib.Path(static_media_folder, media_file_name)))
                    os.makedirs(static_media_folder, exist_ok=True)
                    shutil.copy(media_file_path, static_media_folder)

            # Prepare Hugo attachment table
            repo_url_to_file = urllib.parse.urljoin(GITLAB_REPO_URL, '-/blob/master/')
            media_files_sorted = sorted(media.values(), key=lambda filepath: filepath.name.casefold())
            attachments_list = [f'- <a href="{urllib.parse.urljoin(repo_url_to_file, urllib.parse.quote("/".join((str(self.model_data["xml_path_rel"].parent), media_file.name.strip("/")))))}" target="_blank" rel="noopener noreferrer" title="Show this file in Git repository" data-bs-toggle="tooltip"><i class="fab fa-gitlab pr-1"></i>```{media_file.name}```</a>' for media_file in media_files_sorted] # Encode path/string as URL and join with GitLab base URL; table instead of list: attachments_list = [f'| [```{media_file.name}```]({pathlib.Path(HUGO_MEDIA_WEB_DIR, self.model_data["xml_path_rel"].parent, media_file.name).as_uri().split("file://", 1)[1]}) | – |' for media_file in media] # List of table rows with link to file and description
            attachments_subtable = '\n'.join(attachments_list)
            attachments_table = ( # https://stackoverflow.com/a/53885787/7192373
                '## Downloads\n\n'
                'Files associated with this model:\n\n'
                #'\n| File | Description |\n'
                #'| - | - |\n'
                f'{attachments_subtable}'
            )

        # Save Model ID and XML URL in frontmatter for later rendering in Hugo partial templates
        # Construct Morpheus link
        xml_path_pieces = (HUGO_STATIC_MEDIA_WEB_DIR, model_id.lower(), self.model_data['xml_path_rel'].name) # Hugo baseURL is added by the 'morpheus_link' Hugo shortcode; For full Morpheus link: (MORPHEUS_WEBSITE_BASE_URL.lstrip("htps:/"), HUGO_STATIC_MEDIA_WEB_DIR, model_id, self.model_data['xml_path_rel'].name)
        joined_xml_path = '/'.join(piece.strip('/') for piece in xml_path_pieces)
        safe_xml_path = urllib.parse.quote(joined_xml_path) # Quote reserved characters to obtain a valid URL
        #morpheus_link = ''.join(('morpheus://', joined_xml_path))
        #download_link = ''.join(('https://', joined_xml_path))
        yaml_frontmatter_document.update({'morpheusmodelxml': safe_xml_path})

        # Construct GitLab Repo URL for model folder
        repo_url_to_history = urllib.parse.urljoin(GITLAB_REPO_URL, '-/commits/master/')
        repo_url_to_folder_history = urllib.parse.urljoin(repo_url_to_history, urllib.parse.quote('/'.join((str(self.model_data['xml_path_rel'].parent), '/'))))
        yaml_frontmatter_document.update({'morpheusmodelfolderhistory': repo_url_to_folder_history})

        # Construct GitLab Repo URL for model file
        repo_url_to_xml_history = urllib.parse.urljoin(repo_url_to_history, urllib.parse.quote(str(self.model_data['xml_path_rel'])))
        xml_files = [xml_path for xml_path in self.model_data.get('media').values() if xml_path.suffix.lower() == '.xml'] # Get list/number of XML files in order to determine if the XML files need to be distinguished between a main model and submodels
        if len(xml_files) < 2:
            yaml_frontmatter_document.update({'morpheusmodelxmlhistory': repo_url_to_xml_history})
        else:
            yaml_frontmatter_document.update({'morpheusmodelmainxmlhistory': repo_url_to_xml_history})

        # Open original XML file, not parsed one, to preserve formatting etc.
        with open(self.model_data['xml_path_abs'], 'r') as f:
           xml_file = f.read()

        # Reference section
        publication = self.model_data.get('publication')
        if publication:
            if publication.get('original_model') == True:
                classification_text = 'This model is the **original** used in the publication, up to technical updates:'
            elif publication.get('original_model') == False:
                classification_text = 'This model **reproduces** a published result, originally obtained with a different simulator:'
            else:
                classification_text = ''

            preprint_text = 'The model originates from a **preprint** and thus has not been formally peer-reviewed.'
    
            # Compose Markdown for the article title depending on whether a DOI is given or not
            if 'title' in publication:
                if 'doi' in publication:
                    title = f': [{publication["title"]}]({urllib.parse.urljoin("https://doi.org/", publication["doi"])})'
                else:
                    title = f': {publication["title"]}'
            else:
                title = ''

            # Generate slightly varying reference texts depending on what meta information is given
            reference = ''.join((
                '## Reference\n\n',
                f'{classification_text}\n\n' if 'original_model' in publication else '',
                f'>{", ".join(publication["authors"])}',
                f'{title}',
                    '.' if 'journal' in publication else '',
                f' *{publication["journal"]}*' if 'journal' in publication else '',
                f' **{publication["volume"]}**' if publication.keys() >= {'volume', 'journal'} else '',
                f' ({publication["issue"]})' if publication.keys() >= {'issue', 'volume', 'journal'} else '',
                f': {publication["page"]}' if publication.keys() >= {'page', 'journal'} else '',
                f', {publication["year"]}.\n' if 'year' in publication else '',
                f'\n\n*{preprint_text}*' if publication.get('preprint') == True else '',
            ))
        else:
            reference = ''

        # Box with model download options
        builtin_examples = {
            # Translate repository categories into Morpheus 'Example' menu entries
            'ODE'                   : 'ODE',
            'PDE'                   : 'PDE',
            'Cellular Potts Models' : 'CPM',
            'Multiscale Models'     : 'Multiscale',
            'Miscellaneous Models'  : 'Miscellaneous',
        }

        xml_preview = (
            """{{< spoiler text="<span title='Show XML preview' data-bs-toggle='tooltip'><i class='fas fa-code pr-1'></i>XML Preview</span>" >}}\n"""
            f'```xml\n{xml_file}\n```\n' # XML code block
            '                {{< /spoiler >}}'
        )
        model_download_box = (
            '<div class="card">\n'
            '    <div class="card-header"><i class="fas fa-download fa-lg pr-2"></i>Get this model via:</div>\n'
            '    <div class="card-body" style="padding-top: 0; padding-bottom: 0;">\n'
            '        <div class="card-text" style="padding-top: 0; padding-bottom: 0;">\n'
            '            <p>\n'
            f'                <li>{{{{< morpheus_link "{safe_xml_path}" />}}}} or</li>\n'
            f"""{f'<li> {{{{< icon name="window-restore" pack="fas" padding_right="4" >}}}}<strong>Morpheus GUI</strong>: <code>Examples</code> → <code>{builtin_examples[self.model_data["xml_path_rel"].parent.parts[1]]}</code> → <code>{self.model_data["xml_path_rel"].name}</code> or{chr(10)}</li>' if self.model_data["xml_path_rel"].parent.parts[0] == 'Built-in Examples' else ''}""" # Backslashes are not allowed inside curly braces; https://stackoverflow.com/a/44780467/7192373
            f'                <li>{{{{< model_download "{joined_xml_path}" />}}}}</li>\n'
            '            </p>\n'
            '            <p>\n'
            f'                {xml_preview}\n'
            '            </p>\n'
            '        </div>\n'
            '    </div>\n'
            '</div>'
        )

        # Add alternative spellings to URL aliases
        alternative_url_spellings = set(
            [
                f'/model/{model_id}',
                f'/models/{model_id}',
                f'/models/{model_id.lower()}',
            ]
        )
        yaml_frontmatter_document['aliases'].extend(alternative_url_spellings)

        # Add tags
        tags_auto = set(self.model_data['xml_path_rel'].parent.parts[:-1])
        tags_custom = set(yaml_frontmatter_document['tags'])
        tags_auto_new = tags_auto - tags_custom # Exclude duplicates added by the user from automatically generated tag list
        yaml_frontmatter_document['tags'].extend(tags_auto_new) # Add unique auto-tags
        yaml_frontmatter_document['tags'].sort(key=str.casefold) # Sort after removing all case distinctions in a string

        # Write Markdown file
        filename = 'index.md' # Exactly this file name must be used for featured images to appear in list views; former file name was based on the main XML file: '{:s}.md'.format(self.model_data['xml_path_rel'].stem)
        page_path = pathlib.Path(page_folder, filename) # Full path to Markdown file
        with open(page_path, 'w') as f:
            self.logger.debug('Writing file \'{:s}\'...'.format(os.path.join(self.model_data['xml_path_rel'].parent, filename)))

            # Write YAML front matter and additional, optional, page contents
            yaml.dump_all([yaml_frontmatter_document], stream=f, explicit_start=True)
            content_page = content['page']
            content_model = content['model']
            content = '\n'.join((
                '---' # End of frontmatter
                f'{content_page}' if content_page else '', # Additional content
                f'\n{reference}' if reference else '',
                f'## Model\n',
                f'{model_download_box}',
                f'\n{content_model}\n' if content_model else '', # Optional model.md content
                f'{attachments_table}'
            ))
            f.write(content)

def main():
    # Setup logging, parse commandline arguments etc.
    setup()
    logger = logging.getLogger('main')
    logger.setLevel(logging_level)

    if args.subparser_name:
        # Scan model repository if a subcommand is given
        repo = MorpheusRepository(**directories_abs)

    if args.subparser_name == 'reserve':
        repo.reserve_ids()

    if args.subparser_name == 'build':
        validation_error = repo.check_ids() # Validate IDs
        if validation_error:
            logger.error(validation_error)
            sys.exit(1)

        # Initialise 'Models' folder/section in Hugo
        repo.create_hugo_documentation()

        # Generate model statistics
        if args.stats:
            repo.create_stats()

        # Parse every model and save it in Hugo/Academic format. 
        for model_item in repo.models:
            model = MorpheusModel(model_data=model_item, hugo_dir=directories_abs['hugo_dir'], hugo_asset_dir=directories_abs['hugo_asset_dir'], hugo_static_dir=directories_abs['hugo_static_dir']) # Create model object from repo entry.
            model.markdownify()
        logger.info('Succesfully wrote {:d} model pages to Hugo directory \'{}\'.'.format(len(repo.models), DIRECTORIES['hugo_dir']))

if __name__ == '__main__':
    main()