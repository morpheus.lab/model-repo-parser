# Morpheus models repository parser

Analyses the Morpheus model repository and generates Hugo markdown pages from it.

## Install

We recommend using a virtual environment of your choice. An exemplary installation using ```venv``` is described below.

### venv

	python3 -m venv venv
	source venv/bin/activate

    pip install --upgrade pip
    pip install wheel

    git checkout master
    pip install -r requirements.txt

## Run

    python models.py build --validate

### Command line arguments

To display all options for the model repo parser, type:

    python models.py --help
